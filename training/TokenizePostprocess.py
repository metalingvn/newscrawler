# -*- coding: utf-8 -*-

import os
import re
from os import listdir
from collections import Counter

# Open folder and get all filename
in_folder = os.environ.get('OUTPUT_FOLDER')
out_folder= os.environ.get('POSTPROC_FOLDER')
if os.path.isdir(in_folder):
    # Get file list
    file_list = [f for f in listdir(in_folder)]
    wcounter = Counter()

    # Remove all unnecessary characters
    for filename in file_list:
        data = None
        wordlist = []
        with open(in_folder + "\\" + filename, 'r') as f:
            data = f.readline()

        # Keep only meaningful words
        if(not data is None):
            data = data.replace(" ", "").replace("“", "").replace("”", "").replace("̀", "").replace("…", "").replace("́", "")
            data = re.split('[ (,.;:!?+*#$%&=~^`<>@/)|\\\{\}\[\]\'\"]', data)
            for token in data:
                word = token.strip()
                if((len(word) < 2) or (word.isdigit())):
                    continue
                wordlist.append(word.lower())

        if(len(wordlist) > 0):
            # Save result in %out_folder%
            with open(out_folder + "\\" + filename, 'w') as f:
                f.write(" ".join(wordlist))

            # Update statistics
            wcounter.update(wordlist)

    # Save statistics to file
    if(sum(wcounter.itervalues()) > 0):
        with open(out_folder + "\\statistics.csv", 'w') as f:
            f.write("Word, Count\n")
            for pair in wcounter.most_common(1000):
                f.write(pair[0] + " , " + str(pair[1]) + "\n")