@echo off
cls

REM Folder setting
set INPUT_FOLDER=.\1-rawinput
set OUTPUT_FOLDER=.\2-tokenizedoutput
set POSTPROC_FOLDER=.\3-sanctionoutput

REM Get categories
del /q categories.txt
dir %INPUT_FOLDER% /a:d /b >> categories.txt

REM Clear output folder
echo Initializing output folder..
rd /s /q "%OUTPUT_FOLDER%"
if not exist "%OUTPUT_FOLDER%" mkdir "%OUTPUT_FOLDER%"
rd /s /q "%POSTPROC_FOLDER%"
if not exist "%POSTPROC_FOLDER%" mkdir "%POSTPROC_FOLDER%"

REM Process
echo Begin to tokenize..
for /f "tokens=*" %%i in (categories.txt) do (
    REM Tokinize all file in this folder
    mkdir "%OUTPUT_FOLDER%\%%i"
    java -jar vn.hus.nlp.tokenizer-4.1.1.jar -i "%INPUT_FOLDER%\%%i" -o "%OUTPUT_FOLDER%\%%i"

    REM Rename files
    echo Renaming output files..
    setlocal EnableDelayedExpansion
    set /a FILECOUNT=0
    for %%f in ("%OUTPUT_FOLDER%\%%i"\*.txt) do (
        ren "%%f" "!FILECOUNT!.txt"
        set /a FILECOUNT+=1
    )
    setlocal DisableDelayedExpansion

    REM Normalize all files in folder
    mkdir "%POSTPROC_FOLDER%\%%i"
    Normalize.py -i "%OUTPUT_FOLDER%\%%i" -o "%POSTPROC_FOLDER%\%%i"
)