﻿# -*- coding: utf-8 -*-

import sys, getopt, os,re
from os import listdir

def main(argv):
    in_folder  = ""
    out_folder = ""

    try:
        opts, args = getopt.getopt(argv, "i:o:", ["ifolder=", "ofolder="])
    except getopt.GetoptError:
        print 'Normalize.py -i <inputfolder> -o <inputfolder>'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-i", "--ifolder"):
            in_folder = arg
        elif opt in ("-o", "--ofolder"):
            out_folder = arg

    # Get file list
    file_list = [f for f in listdir(in_folder)]

    # Remove all unnecessary characters
    for filename in file_list:
        data = None
        wordlist = []
        with open(in_folder + "\\" + filename, 'r') as f:
            data = f.readline()

        # Keep only meaningful words
        if(not data is None):
            data = data.replace(" ", " ").replace("“", " ").replace("”", " ").replace("̀", " ").replace("…", " ").replace("́", " ")
            data = re.split('[ (,.;:!?+*#$%&=~^`<>@/)|\\\{\}\[\]\'\"]', data)
            for token in data:
                word = token.strip()
                if((len(word) < 2) or (word.isdigit())):
                    continue
                wordlist.append(word.lower())

        if(len(wordlist) > 0):
            # Save result in %out_folder%
            with open(out_folder + "\\" + filename, 'w') as f:
                f.write(" ".join(wordlist))

            # Update statistics
            # wcounter.update(wordlist)

main(sys.argv[1:])