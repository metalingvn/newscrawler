# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import os
import scrapy
import uuid

#Class chung cho cac bao
class NewsItem(scrapy.Item):
    name = "common_item"

    url     = scrapy.Field()    # Url cua baiviet
    slug    = scrapy.Field()    # Slug words
    time    = scrapy.Field()    # Time
    title   = scrapy.Field()    # Tieude
    summary = scrapy.Field()    # Tomtat
    content = scrapy.Field()    # Noidung (tokenized)

    def MakeSlugFromTitle(self):
        self['slug'] = self['url']
        # TODO: Create slug from title

    def Purify(self):
        self['time']    = ' '.join(self['time'].replace("\r", "").replace("\n", "").replace("\t", "").split())
        self['title']   = ' '.join(self['title'].replace("\r", "").replace("\n", "").replace("\t", "").split())
        self['summary'] = ' '.join(self['summary'].replace("\r", "").replace("\n", "").replace("\t", "").split())
        self['content'] = ' '.join(self['content'].replace("\r", "").replace("\n", "").replace("\t", "").split())

        self.MakeSlugFromTitle()

    def SaveToFile(self):
        # Kiemtra thumuc va tao neu can
        output_folder = self.name + '_output'
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        # Tao file tu noidung
        filename = self['title']
        for c in r'[]/\;,><&*:%=+@!#^()|?^':
            filename = filename.replace(c,'')
        filename = os.path.join(output_folder, filename + ".txt") 
        ifile = open(filename, 'w')
        ifile.write(self['content'].encode('utf-8'))
        ifile.close()


#Bao vnexpress.net
class VnexpressItem(NewsItem):
    name = "vnexpress_item"


# Bao dantri.com.vn
class DantriItem(NewsItem):
    name = "dantri_item"