# -*- coding: utf-8 -*-

import scrapy


from .. import items


class VnexpSpider(scrapy.Spider):
    name = "vnexpress"
    allowed_domains = ["vnexpress.net"]
    url_check_part = "tin-tuc/thoi-su/giao-thong"
    start_urls = ["http://vnexpress.net/tin-tuc/thoi-su/giao-thong"]

    # Tach cac link canthiet
    def parse(self, response):
        # Lay bai moi nhat
        lastnew_url = response.css("div#box_news_top h1.title_news a::attr(href)").extract_first()
        if not lastnew_url is None and self.url_check_part in lastnew_url:
            yield scrapy.Request(lastnew_url, callback = self.parse)

        # Luu noidung baiviet
        sel = response.xpath("//div[@class='block_col_480'][1]")
        if sel:
            item = items.VnexpressItem()
            item["url"]     = response.url
            item["time"]    = ''.join(sel.xpath("//div[@class='block_timer_share']/div[@class='block_timer left txt_666']/text()").extract())
            item["title"]   = ''.join(sel.xpath("//div[@class='title_news']/h1/text()").extract())
            item["summary"] = ''.join(sel.xpath("//h3[@class='short_intro txt_666']/text()").extract())
            item["content"] = ''.join(sel.xpath("//div[@id='left_calculator']/div//text()").extract())
            yield item

        # Lay link phia duoi moi bai
        for href in response.css("div#box_tinkhac_detail a::attr(href)"):
            url = response.urljoin(href.extract())
            if self.url_check_part in url:
                url = url[:url.find("?")]
                yield scrapy.Request(url, callback = self.parse)

        # Lay link quantam cua moi bai
        for href in response.css("div#box_tinlienquan a::attr(href)"):
            url = response.urljoin(href.extract())
            if self.url_check_part in url:
                url = url[:url.find("?")]
                yield scrapy.Request(url, callback = self.parse)


class DantriSpider(scrapy.Spider):
    name = "dantri"
    allowed_domains = ["dantri.com.vn"]
    url_check_part = "/xa-hoi/"
    start_urls = ["http://dantri.com.vn/xa-hoi/giao-thong.htm"]

    # Tach cac link canthiet
    def parse(self, response):
        # Lay link cac bai
        for href in response.css("div.mt3.clearfix a::attr(href)"):
            url = response.urljoin(href.extract())
            if self.url_check_part in url:
                yield scrapy.Request(url, callback = self.parse)

        # Luu noidung baiviet
        sel = response.xpath("//div[@id='ctl00_IDContent_ctl00_divContent']")
        if sel:
            item = items.DantriItem()
            item["url"]     = response.url
            item["time"]    = ''.join(sel.xpath("//span[@class='fr fon7 mr2 tt-capitalize']/text()").extract())
            item["title"]   = ''.join(sel.xpath("//h1/text()").extract())
            item["summary"] = ''.join(sel.xpath("//h2[@class='fon33 mt1 sapo']/text()").extract())
            item["content"] = ''.join(sel.xpath("//div[@id='divNewsContent']//text()").extract())
            yield item

        # Lay link phia duoi moi bai
        for href in response.css("div#divOlderNews a::attr(href)"):
            url = response.urljoin(href.extract())
            if self.url_check_part in url:
                yield scrapy.Request(url, callback = self.parse)